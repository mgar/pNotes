/**
 * (c) Owner of https://codeberg.org/mgar
 */
import React, { useState } from 'react';
import styles from './styles.module.css';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';

interface Props {
  branchSuffix: string;
}

export default function GitCommands({
  branchSuffix,
}: Props): JSX.Element {

  const [copySuccess, setCopySuccess] = useState('');
  function copyToClipboard(parameter) {
    let fullCommand = 'git '+parameter.param;
    if(parameter.needsBranch){
      fullCommand+=' '+branchPrefix+branchSuffix;
    }
    navigator.clipboard.writeText(fullCommand);
    console.log(fullCommand);
    setCopySuccess('✔');
    const timer = setTimeout(() => setCopySuccess(''), 1000);
    return () => clearTimeout(timer);
  };

  const parameters = [
    {param:"status"},
    {param:"checkout main"},
    {param:"pull"},
    {param:"switch", needsBranch:true},
    {param:"checkout -b", needsBranch:true},
    {param:"push origin", needsBranch:true},
  ];
  const {siteConfig} = useDocusaurusContext();
  const {customFields} = siteConfig;
  const branchPrefix = customFields.gitBranchPrefix;
  const commandItems = parameters.map((parameter) => {
    if(parameter.needsBranch)
      return<span className={styles.gitCommandLine} key={parameter.param} onClick={() => copyToClipboard(parameter)}>
        <span className={styles.gitCommandCopy}>&nbsp;</span>
        <span className={styles.gitCommandMain} >
          git&nbsp;
        </span>
        <span>{parameter.param}&nbsp;</span>
        <span className={styles.gitCommandBranch}>{branchPrefix}{branchSuffix}</span>
      </span>
    return <span className={styles.gitCommandLine} key={parameter.param} onClick={() => copyToClipboard(parameter)}>
        <span className={styles.gitCommandCopy}>&nbsp;</span>
        <span className={styles.gitCommandMain}>
          git&nbsp;
        </span>
        <span>{parameter.param}</span>
      </span>
  });
  return (
    <div>
      <pre className={styles.gitCommand}>
        <code>
          <span className={styles.gitCopiedOk}>{copySuccess}</span>
          {commandItems}
        </code>
      </pre>
    </div>
  );
}
