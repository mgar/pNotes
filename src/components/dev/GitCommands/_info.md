<!---
  (c) Owner of https://codeberg.org/mgar
-->
import GitCommands from '@site/src/components/dev/GitCommands';
import BrowserWindow from '@site/src/components/BrowserWindow';

export const demoId = 'ITERATION-ID';

This component provides a list of Git Common Commands used in development tasks
The prefix for the branch can optionally be defined in a custom field

```js title="docusaurus.config.js"
 module.exports = {
  // ...
  customFields: {
    gitBranchPrefix: 'BRANCH/PREFIX/',
  },
  // ...
};
```
Example using the [Front Matter](https://docusaurus.io/docs/next/create-doc#doc-front-matter)`.id` as the branch suffix
```md title="/docs/[teamId]/[iterationsContainerId]/[iterationId]/[increment.mdx]"
import GitCommands from '@site/src/components/dev/GitCommands';

<GitCommands branchSuffix={frontMatter.id}/>
```

<BrowserWindow>
    <GitCommands branchSuffix={demoId}/>
</BrowserWindow>


<GitCommands branchSuffix={demoId}/>
