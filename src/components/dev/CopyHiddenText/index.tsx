/**
 * (c) Owner of https://codeberg.org/mgar
 */
import React, { useRef, useState } from 'react';
import styles from './styles.module.css';
import CopiarOculto from '../img/copiar-oculto.svg';

interface Props {
  label: string;
  value: string;
}

export default function CopyHiddenText({
  label,
  value,
}: Props) {

  const [copySuccess, setCopySuccess] = useState('');

  function copyToClipboard(e) {
     navigator.clipboard.writeText(value);
     setCopySuccess('✔');
     const timer = setTimeout(() => setCopySuccess(''), 3000);
     return () => clearTimeout(timer);
  };

  return (
    <span className={styles.copyHiddenText} onClick={copyToClipboard}>
      <label>{label}</label>
        <span>
          <CopiarOculto className={styles.copyHiddenTextIcon}/>
          <span>{copySuccess}</span>
        </span>
    </span>
  );
}
