<!---
  (c) Owner of https://codeberg.org/mgar
-->
import CopyHiddenText from '@site/src/components/dev/CopyHiddenText';
import BrowserWindow from '@site/src/components/BrowserWindow';


This component sets a hidden value that can be copied to clipboard

**Note!** the value is stored as plain text in code

```md title="/docs/[teamId]/[iterationsContainerId]/[iterationId]/[increment.mdx]"
import DevItemInfo from '@site/src/components/dev/DevItemInfo';

<CopyHiddenText label="Not a secure value!" value="Copied!"/>
```

<BrowserWindow>
    <CopyHiddenText label="Not a secure value!" value="Copied!"/>
</BrowserWindow>
