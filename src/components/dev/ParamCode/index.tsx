/**
 * (c) Owner of https://codeberg.org/mgar
 */
import React, { useRef, useEffect, useState } from 'react';
import styles from './styles.module.css';
import CopiarOculto from '../img/copiar-oculto.svg';

interface Props {
  params: Map<string, string>;
  showParams: boolean;
  children: JSX.Element;
}

export default function ParamCode({
  params,
  showParams,
  children,
}: Props): JSX.Element {

  // intercept code block
  console.log(children.props);
  
  if(children && children.props && children.props.children){
    let code = children.props.children;
    Object.keys(params).forEach(key => {
      code = code.replaceAll('{'+key+'}', params[key]);
    });
    children=React.cloneElement(children, [], code);
  }

  const getparamsContent = params => {
    let content = [];
    Object.keys(params).forEach(key => {
      content.push(<li key={key}>{key} <code>{params[key]}</code></li>);
    });
    return content;
  };

  let paramDiv = '';
  if(showParams){
     paramDiv = <ul>
       {getparamsContent(params)}
     </ ul>
  };

  return (
    <span>
      {paramDiv}
      <span>{children}</span>
    </span>
  );
}
