<!---
  (c) Owner of https://codeberg.org/mgar
-->
import ParamCode from '@site/src/components/dev/ParamCode';
import BrowserWindow from '@site/src/components/BrowserWindow';
import PythonSampleSource from '!!raw-loader!./sample.py';
import CodeBlock from '@theme/CodeBlock';


This component can replace strings in CodeBlock

```js
import ParamCode from '@site/src/components/dev/ParamCode';
import PythonSampleSource from '!!raw-loader!./sample.py';

// Parameters that will be replaced
export const params = {
  "param1":"value1",
  "param2":"value2"
}
// True it the parameters will be rendered
export const showParams = true;

<ParamCode params={params} showParams={showParams}>
    <CodeBlock
      language="python"
      title="Python Source Example">
      {PythonSampleSource}
    </CodeBlock>
</ParamCode>
```
export const params = {
  "param1":"value1",
  "param2":"value2"
};

export const showParams = true;

<BrowserWindow>
  <ParamCode params={params} showParams={showParams}>
      <CodeBlock
        language="python"
        title="Python Source Example">
        {PythonSampleSource}
      </CodeBlock>
  </ParamCode>
</BrowserWindow>
