/**
 * (c) Owner of https://codeberg.org/mgar
 */
import React, {type ReactNode} from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';

interface Props {
  minHeight: number;
  id: string;
}

export default function DevItemInfo({
  minHeight,
  id,
}: Props): JSX.Element {
  const {siteConfig} = useDocusaurusContext();
  const {customFields} = siteConfig;
  const infoLink = customFields.itemInfo.urlPrefix+id;
  return (
    <a href={infoLink} target="blank" rel="noopener noreferrer">
      <div className={clsx(styles.devItemInfo, 'item shadow--md')} style={{minHeight}}>
        <div className={styles.devItemInfoIcon}>
          <img id="icon" src={customFields.itemInfo.iconUrl}/>
        </div>
        <div className={styles.devItemInfoLink}>
          {id}
        </div>
      </div>
    </a>
  );
}
