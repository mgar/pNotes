<!---
  (c) Owner of https://codeberg.org/mgar
-->
import DevItemInfo from '@site/src/components/dev/DevItemInfo';
import BrowserWindow from '@site/src/components/BrowserWindow';

export const demoId = 'ITERATION-ID';

This component displays an icon and the url to the page with information
about the increment (User Story, Ticket, etc...)

```js title="docusaurus.config.js"
 module.exports = {
  // ...
  customFields: {
    itemInfo: {
      iconUrl: 'https://www.mediawiki.org/static/images/mobile/copyright/mediawiki.svg',
      urlPrefix: 'https://example.org/',
    },
  },
  // ...
};
```
Example using the [Front Matter](https://docusaurus.io/docs/next/create-doc#doc-front-matter)`.id` as the branch suffix
```md title="/docs/[teamId]/[iterationsContainerId]/[iterationId]/[increment.mdx]"
import DevItemInfo from '@site/src/components/dev/DevItemInfo';

<DevItemInfo id="{frontMatter.id}"/>
```

<BrowserWindow>
    <DevItemInfo id="ITERATION-ID"/>
</BrowserWindow>
