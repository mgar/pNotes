# pNotes

This project is used to maintain **personal** notes about development projects, main documentation should be on business documentation system (i.e. wikis), but code examples and links to tasks and official information can be maintained here.
It is built using [Docusaurus 3](https://docusaurus.io/).

### Installation

Install nodejs version 18+

Install NVM if needed

```
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

nvm install 18

nvm use 18
```



### Local Development

```
$ npm start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
$ npm run build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.
