---
sidebar_position: 1
---

# Team Template

The team template is a JSON file that defines the generation of the initial page for the team.

```json title="/docs/[teamId]/_category.json"
{
  "label": "Team Name",
  "position": 2,
  "link": {
    "type": "generated-index",
    "description": "Team Description"
  }
}
```

The folder for this Team also contains the folders of other [iteration containers](./iterations-container)
