---
sidebar_position: 3
---

# Iteration Template

The iteration template is a JSON file that defines the generation of the initial page for an **iteration**. It can be a time container like a month or the name of a sccrum sprint.

```json title="/docs/[teamId]/[iterationsContainerId]/[iterationId]/_category.json"
{
  "label": "Iteration 00",
  "position": 2,
  "link": {
    "type": "generated-index",
    "description": "Iteration Container"
  }
}
```

The folder for this iteration also contains the [increment](/docs/category/increment-templates)
