---
sidebar_position: 2
---

# Iterations Container Template

The team template is a JSON file that defines the generation of the initial page for an **iterations container**. It can be a time container like a year or the name of a release.

```json title="/docs/[teamId]/[iterationsContainerId]/_category.json"
{
  "label": "Iterations Container 00",
  "position": 2,
  "link": {
    "type": "generated-index",
    "description": "Iteration Container name"
  }
}
```

The folder for this Iterations Container also contains the folders of other [iterations](./iteration)
